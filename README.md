# dune-iwr


# [`dune-core-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git) metapackage

| `README.md`                                                                                                                        | Latest build from [GitHub](https://github.com/dune-project)'s package (DD/MM/YY) | `pkg.tar.zst` package                                        |
| :--------------------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------: | :----------------------------------------------------------- |
| [`dune-common-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git/-/blob/main/dune-common-git/README.md)                 |                                    19/05/2021                                    | `dune-common-git-10814.b2a3e8ad5-1-x86_64.pkg.tar.zst`       |
| [`dune-geometry-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git/-/blob/main/dune-geometry-git/README.md)             |                                    19/05/2021                                    | `dune-geometry-git-1100.ea31bc2-1-x86_64.pkg.tar.zst`        |
| [`dune-grid-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git/-/blob/main/dune-grid-git/README.md)                     |                                    19/05/2021                                    | `dune-grid-git-11025.c1c164dac-1-x86_64.pkg.tar.zst`         |
| [`dune-istl-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git/-/blob/main/dune-istl-git/README.md)                     |                                    19/05/2021                                    | ` dune-istl-git-3359.d6293dac-1-x86_64.pkg.tar.zst`          |
| [`dune-localfunctions-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-core-git/-/blob/main/dune-localfunctions-git/README.md) |                                    19/05/2021                                    | `dune-localfunctions-git-1748.a97a6006-1-x86_64.pkg.tar.zst` |

### Tests

OK
